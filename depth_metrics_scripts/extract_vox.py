# /Applications/Blender.app/Contents/MacOS/Blender ../2011_09_30_drive_0033_sync_vox.blend --background --python extract_vox.py

import os
import bpy

# ---------------------------------
# Create structure if not existing
# def create_dir(path):

#     if not os.path.exists(path):
#         os.makedirs(path_vox)    


path_vox = 'vox_depth'
path_left = os.path.join(path_vox, '02')
path_right = os.path.join(path_vox, '03')

# create_dir(path_vox)
# create_dir(path_left)
# create_dir(path_right)

# ---------------------------------
# Shortcuts
bpy.context.scene.use_nodes = True

scene   = bpy.context.scene
tree    = scene.node_tree
links   = tree.links

# ---------------------------------
# Enable compositing


# Clear default nodes
for n in tree.nodes:
    tree.nodes.remove(n)

scene.render.image_settings.file_format = 'OPEN_EXR'

# ---------------------------------
# Prepare nodes

rl = tree.nodes.new('CompositorNodeRLayers')
output = tree.nodes.new('CompositorNodeComposite') 


links.new(rl.outputs[2], output.inputs[0])

# ---------------------------------
number_of_frames = scene.frame_end - scene.frame_start + 1

print('Number of frames: {}'.format(number_of_frames))

# Get cameras
cameras = {
  '02'    : scene.objects['Camera 2'],
  # '03'    : scene.objects['Camera 3'],
}

# Loop over camera
for c, bpy_c in cameras.items():

    scene.camera = bpy_c

    # Loop over frames
    for f in range(306, 606):

        # if not f == 400:
        #     continue

        # if f < 600:
        #     continue

        # if f < 597:
        #     continue

        if not f%10 == 0:
            continue

        scene.frame_set(f)

        # Extract depth
        scene.render.filepath = os.path.join("vox_depth", c, '{:010d}'.format(f))
        bpy.ops.render.render(write_still=True)   